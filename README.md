# Assignment 3 - Sarav Patel
# Ghost Story Chat Bot

## Introduction

* This is responsive chatbot which mostly like by kids. In this chatbot i am providing one story telling game and its related to Ghost.
* This is Halloween Season so people like to play this small game with diffrent kind of senario and next step is depends on your Answer. So, Be aware Ghost are everywhere.
---

## Technology Listing:

* Node.js
* HTML
* JQuery
* CSS
* JavaScript




---

## Required Softwears

* Visual Studio Code - https://code.visualstudio.com/download
* Node.js - https://nodejs.org/en/download/
* Heroku - https://id.heroku.com/login

---

## Setup

### Installation process 

```
First you need to download heroku CLI.
heroku login
```

### Clone the repository

```
heroku git:clone -a projectname
cd ghostchatbot
```

### Deploy your Changes

```
git add .
git commit -am "make it better"
git push heroku master
```

### Access Web Browser

```
https://ghostchatbot.herokuapp.com/
```
---

## License

Copyright 2020 @Sarav Patel
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
